// アコーディオン

$(function() {
	$('.accordion').click(function() {
		var target = $(this);
		if(!!target.hasClass('opened')) {
			target.removeClass('opened');
		} else {
			target.addClass('opened');
		}
		target.next().slideToggle();
	});
});