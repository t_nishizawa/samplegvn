$(function() {
	var showFlag = false;
	var topBtn = $('#page-top');
	topBtn.css('bottom', '-250px');
	var showFlag = false;
	//スクロールが100に達したらボタン表示
	$(window).scroll(function () {
		if ($(this).scrollTop() > 500) {
			if (showFlag == false) {
				showFlag = true;
				topBtn.stop().animate({'bottom' : '0'}, 100);
			}
		} else {
			if (showFlag) {
				showFlag = false;
				topBtn.stop().animate({'bottom' : '-250px'}, 100);
			}
		}
	});
	//スクロールしてトップ
    topBtn.click(function () {
		$('body,html').animate({
			scrollTop: 0
		}, 500);
		return false;
    });
});